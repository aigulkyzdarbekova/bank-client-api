package com.solvo.contragent.dto;
import com.solvo.contragent.interfaces.AbstractDTO;
import lombok.*;


@Builder
@Getter @Setter
@AllArgsConstructor
@NoArgsConstructor(force = true)
public class BankTransactionDTO extends AbstractDTO {

    private int accountFrom;
    private int accountTo;

    private String currencyCategory;
    private String expenseCategory;

    private double transactionSum;
}
