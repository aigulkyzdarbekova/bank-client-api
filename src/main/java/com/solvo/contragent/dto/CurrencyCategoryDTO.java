package com.solvo.contragent.dto;


import com.solvo.contragent.interfaces.AbstractDTO;
import lombok.*;

@Builder
@Getter @Setter
@AllArgsConstructor
@NoArgsConstructor(force = true)
public class CurrencyCategoryDTO extends AbstractDTO {
    private String title;
}
