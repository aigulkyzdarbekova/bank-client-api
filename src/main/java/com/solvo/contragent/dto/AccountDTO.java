package com.solvo.contragent.dto;
import com.solvo.contragent.interfaces.AbstractDTO;
import lombok.*;

import java.time.LocalDateTime;


@Builder
@Getter @Setter
@AllArgsConstructor
@NoArgsConstructor(force = true)
public class AccountDTO extends AbstractDTO {

    private int number;

    private double limitSum;

    private LocalDateTime limitDate;

    private String expenseCategory;
}
