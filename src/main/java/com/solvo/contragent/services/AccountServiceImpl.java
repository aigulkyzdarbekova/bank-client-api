package com.solvo.contragent.services;

import com.solvo.contragent.models.Account;
import com.solvo.contragent.repositories.AccountRepository;
import com.solvo.contragent.services.interfaces.AccountService;
import org.springframework.stereotype.Service;

@Service
public class AccountServiceImpl implements AccountService{
    private final AccountRepository accountRepository;

    public AccountServiceImpl(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }


    @Override
    public Account read(Integer number) {
        return accountRepository.findByNumber(number);
    }

    @Override
    public Account create(Account account) {
        return accountRepository.save(account);
    }
}
