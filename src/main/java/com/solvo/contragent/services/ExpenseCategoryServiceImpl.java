package com.solvo.contragent.services;
import com.solvo.contragent.models.ExpenseCategory;
import com.solvo.contragent.repositories.ExpenseCategoryRepository;
import com.solvo.contragent.services.interfaces.ExpenseCategoryService;
import org.springframework.stereotype.Service;

@Service
public class ExpenseCategoryServiceImpl implements ExpenseCategoryService {
    private final ExpenseCategoryRepository expenseCategoryRepository;

    public ExpenseCategoryServiceImpl(ExpenseCategoryRepository expenseCategoryRepository) {
        this.expenseCategoryRepository = expenseCategoryRepository;
    }

    @Override
    public ExpenseCategory create(ExpenseCategory expenseCategory) {
        return expenseCategoryRepository.save(expenseCategory);
    }

    @Override
    public ExpenseCategory read(String title) {
        return expenseCategoryRepository.findByTitle(title);
    }
}
