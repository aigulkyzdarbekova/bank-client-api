package com.solvo.contragent.services;

import com.solvo.contragent.models.CurrencyCategory;
import com.solvo.contragent.repositories.CurrencyCategoryRepository;
import com.solvo.contragent.services.interfaces.CurrencyCategoryService;
import org.springframework.stereotype.Service;

@Service
public class CurrencyCategoryServiceImpl implements CurrencyCategoryService{
    private final CurrencyCategoryRepository currencyCategoryRepository;


    public CurrencyCategoryServiceImpl(CurrencyCategoryRepository currencyCategoryRepository) {
        this.currencyCategoryRepository = currencyCategoryRepository;
    }


    @Override
    public CurrencyCategory create(CurrencyCategory currencyCategory) {
        return currencyCategoryRepository.save(currencyCategory);
    }

    @Override
    public CurrencyCategory read(String title) {
        return currencyCategoryRepository.findByTitle(title);
    }
}
