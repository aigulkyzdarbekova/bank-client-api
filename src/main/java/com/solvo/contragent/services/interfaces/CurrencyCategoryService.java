package com.solvo.contragent.services.interfaces;

import com.solvo.contragent.models.CurrencyCategory;


public interface CurrencyCategoryService {
    CurrencyCategory create(CurrencyCategory currencyCategory);
    CurrencyCategory read(String title);
}
