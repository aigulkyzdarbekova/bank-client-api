package com.solvo.contragent.services.interfaces;
import com.solvo.contragent.models.BankTransaction;


public interface BankTransactionService {
    BankTransaction create(BankTransaction transaction);

}
