package com.solvo.contragent.services.interfaces;
import com.solvo.contragent.models.ExpenseCategory;

public interface ExpenseCategoryService {
    ExpenseCategory create(ExpenseCategory expenseCategory);
    ExpenseCategory read(String title);
}
