package com.solvo.contragent.services.interfaces;

import com.solvo.contragent.models.Account;


public interface AccountService {
    Account read(Integer number);
    Account create(Account account);
}
