package com.solvo.contragent.services;

import com.solvo.contragent.models.Account;
import com.solvo.contragent.models.BankTransaction;
import com.solvo.contragent.repositories.BankTransactionRepository;
import com.solvo.contragent.services.interfaces.AccountService;
import com.solvo.contragent.services.interfaces.ExpenseCategoryService;
import com.solvo.contragent.services.interfaces.BankTransactionService;
import org.springframework.stereotype.Service;



@Service
public class BankTransactionServiceImpl implements BankTransactionService {
    private final BankTransactionRepository transactionRepository;
    private final AccountService accountService;
    private final ExpenseCategoryService expenseCategoryService;

    public BankTransactionServiceImpl(BankTransactionRepository transactionRepository, AccountService accountService, ExpenseCategoryService expenseCategoryService) {
        this.transactionRepository = transactionRepository;
        this.accountService = accountService;
        this.expenseCategoryService = expenseCategoryService;
    }

    @Override
    public BankTransaction create(BankTransaction transaction) {
        Account account = accountService.read(transaction.getAccountFrom());
        int expenseId = transaction.getExpenseId().getId();

        double limitBalance = account.getLimitSum() - transaction.getTransactionSum();
        transaction.setLimitBalance(limitBalance);

        if(expenseId == account.getExpenseCategory().getId()){
            if(transaction.getLimitBalance() < 0){
                transaction.setLimitExceeded(true);
            }

        }

        return transactionRepository.save(transaction);
    }

}
