package com.solvo.contragent.controllers.banktransaction;

import com.solvo.contragent.dto.BankTransactionDTO;
import com.solvo.contragent.mapper.BankTransactionMapper;
import com.solvo.contragent.services.interfaces.BankTransactionService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/api/transactions")
public class CreateBankTransactionController {
    private final BankTransactionService service;
    private final BankTransactionMapper mapper;

    public CreateBankTransactionController(BankTransactionService service, BankTransactionMapper mapper) {
        this.service = service;
        this.mapper = mapper;
    }


    @PostMapping
    public BankTransactionDTO create(@RequestBody BankTransactionDTO transactionDTO) {
        return mapper.toDTO(service.create(mapper.toEntity(transactionDTO)));
    }
}
