package com.solvo.contragent.controllers.account;

import com.solvo.contragent.dto.AccountDTO;
import com.solvo.contragent.mapper.AccountMapper;
import com.solvo.contragent.services.interfaces.AccountService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/api/accounts")
public class CreateAccountController {
    private final AccountService service;
    private final AccountMapper mapper;

    public CreateAccountController(AccountService service, AccountMapper mapper) {
        this.service = service;
        this.mapper = mapper;
    }


    @PostMapping
    public AccountDTO create(@RequestBody AccountDTO accountDTO) {
        return mapper.toDTO(service.create(mapper.toEntity(accountDTO)));
    }
}
