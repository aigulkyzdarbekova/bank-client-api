package com.solvo.contragent.controllers.currencycategory;

import com.solvo.contragent.dto.CurrencyCategoryDTO;
import com.solvo.contragent.mapper.CurrencyCategoryMapper;
import com.solvo.contragent.services.interfaces.CurrencyCategoryService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/api/currency_categories")
public class CreateCurrencyCategoryController {
    private final CurrencyCategoryService service;
    private final CurrencyCategoryMapper mapper;

    public CreateCurrencyCategoryController(CurrencyCategoryService service, CurrencyCategoryMapper mapper) {
        this.service = service;
        this.mapper = mapper;
    }


    @PostMapping
    public CurrencyCategoryDTO create(@RequestBody CurrencyCategoryDTO currencyCategoryDTO) {
        return mapper.toDTO(service.create(mapper.toEntity(currencyCategoryDTO)));
    }
}
