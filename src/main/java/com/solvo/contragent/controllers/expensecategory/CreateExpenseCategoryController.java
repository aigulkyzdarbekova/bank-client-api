package com.solvo.contragent.controllers.expensecategory;

import com.solvo.contragent.dto.ExpenseCategoryDTO;
import com.solvo.contragent.mapper.ExpenseCategoryMapper;
import com.solvo.contragent.services.interfaces.ExpenseCategoryService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/api/expense_categories")
public class CreateExpenseCategoryController {
    private final ExpenseCategoryService service;
    private final ExpenseCategoryMapper mapper;

    public CreateExpenseCategoryController(ExpenseCategoryService service, ExpenseCategoryMapper mapper) {
        this.service = service;
        this.mapper = mapper;
    }


    @PostMapping
    public ExpenseCategoryDTO create(@RequestBody ExpenseCategoryDTO expenseCategoryDTO) {
        return mapper.toDTO(service.create(mapper.toEntity(expenseCategoryDTO)));
    }
}
