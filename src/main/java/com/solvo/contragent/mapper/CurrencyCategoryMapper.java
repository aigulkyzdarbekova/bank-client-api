package com.solvo.contragent.mapper;

import com.solvo.contragent.interfaces.AbstractMapper;
import com.solvo.contragent.dto.CurrencyCategoryDTO;
import com.solvo.contragent.models.CurrencyCategory;
import org.springframework.stereotype.Component;

@Component
public class CurrencyCategoryMapper extends AbstractMapper<CurrencyCategory, CurrencyCategoryDTO> {
    public CurrencyCategoryMapper() {
        super(CurrencyCategory.class, CurrencyCategoryDTO.class);
    }
}