package com.solvo.contragent.mapper;

import com.solvo.contragent.interfaces.AbstractMapper;
import com.solvo.contragent.dto.BankTransactionDTO;
import com.solvo.contragent.models.CurrencyCategory;
import com.solvo.contragent.models.ExpenseCategory;
import com.solvo.contragent.models.BankTransaction;
import com.solvo.contragent.services.interfaces.CurrencyCategoryService;
import com.solvo.contragent.services.interfaces.ExpenseCategoryService;
import com.solvo.contragent.services.interfaces.BankTransactionService;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;
import javax.annotation.PostConstruct;
import java.util.Objects;

@Component
public class BankTransactionMapper extends AbstractMapper<BankTransaction, BankTransactionDTO> {
    private final ModelMapper mapper;
    private final BankTransactionService transactionService;
    private final CurrencyCategoryService currencyCategoryService;
    private final ExpenseCategoryService expenseCategoryService;

    public BankTransactionMapper(ModelMapper mapper, BankTransactionService transactionService, CurrencyCategoryService currencyCategoryService, ExpenseCategoryService expenseCategoryService) {
        super(BankTransaction.class, BankTransactionDTO.class);
        this.mapper = mapper;
        this.transactionService = transactionService;
        this.currencyCategoryService = currencyCategoryService;
        this.expenseCategoryService = expenseCategoryService;
    }


    @PostConstruct
    public void setupMapper(){
        mapper
                .createTypeMap(BankTransaction.class, BankTransactionDTO.class)
                .addMappings(mapper -> {
                    mapper.skip(BankTransactionDTO::setCurrencyCategory);
                    mapper.skip(BankTransactionDTO::setExpenseCategory);
                })
                .setPostConverter(toDTOConverter());
        mapper
                .createTypeMap(BankTransactionDTO.class, BankTransaction.class)
                .addMappings(mapper -> mapper.skip(BankTransaction::setCurrencyId))
                .addMappings(mapper -> mapper.skip(BankTransaction::setExpenseId))
                .setPostConverter(toEntityConverter());
    }


    @Override
    public void mapSpecificFields(BankTransaction source, BankTransactionDTO destination){
        destination.setCurrencyCategory(source.getCurrencyId().getTitle());
        destination.setExpenseCategory(source.getExpenseId().getTitle());
    }

    @Override
    public void mapSpecificFields(BankTransactionDTO source, BankTransaction destination){
        String currencyCategoryTitle = getCurrencyCategory(source);
        CurrencyCategory currencyCategory = currencyCategoryService.read(currencyCategoryTitle);

        destination.setCurrencyId(currencyCategory);


        String expenseCategoryTitle = getExpenseCategory(source);
        ExpenseCategory expenseCategory = expenseCategoryService.read(expenseCategoryTitle);

        destination.setExpenseId(expenseCategory);
    }


    private String getCurrencyCategory (BankTransactionDTO transactionDTO){
        return Objects.isNull(transactionDTO)
                ? null
                :transactionDTO
                .getCurrencyCategory();
    }

    private String getExpenseCategory (BankTransactionDTO transactionDTO){
        return Objects.isNull(transactionDTO)
                ? null
                :transactionDTO
                .getExpenseCategory();
    }

}
