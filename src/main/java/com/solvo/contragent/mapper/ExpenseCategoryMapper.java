package com.solvo.contragent.mapper;

import com.solvo.contragent.interfaces.AbstractMapper;
import com.solvo.contragent.dto.ExpenseCategoryDTO;
import com.solvo.contragent.models.ExpenseCategory;
import org.springframework.stereotype.Component;

@Component
public class ExpenseCategoryMapper extends AbstractMapper<ExpenseCategory, ExpenseCategoryDTO> {
    public ExpenseCategoryMapper() {
        super(ExpenseCategory.class, ExpenseCategoryDTO.class);
    }
}