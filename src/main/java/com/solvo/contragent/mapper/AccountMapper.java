package com.solvo.contragent.mapper;

import com.solvo.contragent.dto.AccountDTO;
import com.solvo.contragent.interfaces.AbstractMapper;
import com.solvo.contragent.models.Account;
import com.solvo.contragent.models.ExpenseCategory;
import com.solvo.contragent.services.interfaces.AccountService;
import com.solvo.contragent.services.interfaces.ExpenseCategoryService;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Objects;

@Component
public class AccountMapper extends AbstractMapper<Account, AccountDTO> {
    private final ModelMapper mapper;
    private final ExpenseCategoryService expenseCategoryService;

    public AccountMapper(ModelMapper mapper, ExpenseCategoryService expenseCategoryService) {
        super(Account.class, AccountDTO.class);
        this.mapper = mapper;
        this.expenseCategoryService = expenseCategoryService;
    }


    @PostConstruct
    public void setupMapper(){
        mapper
                .createTypeMap(Account.class, AccountDTO.class)
                .addMappings(mapper -> {
                    mapper.skip(AccountDTO::setExpenseCategory);
                })
                .setPostConverter(toDTOConverter());
        mapper
                .createTypeMap(AccountDTO.class, Account.class)
                .addMappings(mapper -> mapper.skip(Account::setExpenseCategory))
                .setPostConverter(toEntityConverter());
    }


    @Override
    public void mapSpecificFields(Account source, AccountDTO destination){
        destination.setExpenseCategory(source.getExpenseCategory().getTitle());
    }

    @Override
    public void mapSpecificFields(AccountDTO source, Account destination){
        String expenseCategoryTitle = getExpenseCategory(source);
        ExpenseCategory expenseCategory = expenseCategoryService.read(expenseCategoryTitle);

        destination.setExpenseCategory(expenseCategory);
    }


    private String getExpenseCategory (AccountDTO accountDTO){
        return Objects.isNull(accountDTO)
                ? null
                :accountDTO
                .getExpenseCategory();
    }

}
