package com.solvo.contragent.repositories;
import com.solvo.contragent.models.CurrencyCategory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface CurrencyCategoryRepository extends JpaRepository<CurrencyCategory, Integer>, JpaSpecificationExecutor {
    CurrencyCategory findByTitle(String title);
}
