package com.solvo.contragent.repositories;

import com.solvo.contragent.models.CurrentCurrency;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CurrentCurrencyRepository extends JpaRepository<CurrentCurrency, Integer> {
}
