package com.solvo.contragent.repositories;
import com.solvo.contragent.models.ExpenseCategory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface ExpenseCategoryRepository extends JpaRepository<ExpenseCategory, Integer> {
    ExpenseCategory findByTitle(String title);

}
