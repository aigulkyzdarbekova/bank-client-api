package com.solvo.contragent.repositories;
import com.solvo.contragent.models.BankTransaction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BankTransactionRepository extends JpaRepository<BankTransaction, Integer> {
}
