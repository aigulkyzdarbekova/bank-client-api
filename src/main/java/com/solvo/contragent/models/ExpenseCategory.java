package com.solvo.contragent.models;
import com.solvo.contragent.interfaces.AbstractEntity;
import lombok.*;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Getter @Setter
@AllArgsConstructor
@NoArgsConstructor(force = true)
@Table(name = "expense_categories")
public class ExpenseCategory extends AbstractEntity {

    private String title;
}
