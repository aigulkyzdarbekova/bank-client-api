package com.solvo.contragent.models;

import com.solvo.contragent.interfaces.AbstractEntity;
import lombok.*;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Entity
@Getter @Setter
@AllArgsConstructor
@NoArgsConstructor(force = true)
@Table(name = "transactions")
public class BankTransaction extends AbstractEntity {

    private int accountFrom;
    private int accountTo;

    private LocalDateTime transactionDate;

    private double transactionSum;

    private boolean limitExceeded;

    @ManyToOne
    @JoinColumn(name = "currency_id")
    private CurrencyCategory currencyId;

    @ManyToOne
    @JoinColumn(name = "expense_id")
    private ExpenseCategory expenseId;

}
