package com.solvo.contragent.models;

import com.solvo.contragent.interfaces.AbstractEntity;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Entity
@Getter @Setter
@AllArgsConstructor
@NoArgsConstructor(force = true)
@Table(name = "accounts")
public class Account extends AbstractEntity {

    private int number;

    private double limitProduct;
    private double limitService;

    private LocalDateTime limitDateProduct;
    private LocalDateTime limitDateService;

    private double sum;
    private double limitBalance;

    @ManyToOne
    @JoinColumn(name = "expense_id")
    private ExpenseCategory ExpenseCategory;



}
