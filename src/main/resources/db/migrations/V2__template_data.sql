INSERT INTO  currency_categories (created, updated, title)
VALUES ('2022-12-11T19:10:25-07', '2022-12-11T20:10:25-07', 'USD'),
       ('2022-12-10T16:10:25-07', '2022-12-11T19:10:25-07', 'KAZ'),
       ('2022-12-10T10:10:25-07', '2022-12-11T11:10:25-07', 'RUB');

INSERT INTO expense_categories (created, updated, title)
VALUES ('2022-12-10T20:10:25-07', '2022-12-11T20:10:25-07', 'услуги'),
       ('2022-12-10T20:10:25-07', '2022-12-11T20:10:25-07', 'товары');
