--Модуль виды валют.

CREATE TABLE currency_categories
(
    "id"          	    SERIAL       NOT NULL,
    "created"     	    TIMESTAMP    NOT NULL,
    "updated"     	    TIMESTAMP    DEFAULT NULL,
    "title"   	        VARCHAR      NOT NULL,

    CONSTRAINT currency_categories_pkey
        PRIMARY KEY(id)
);


--Модуль виды операции.

CREATE TABLE expense_categories
(
    "id"          	    SERIAL       NOT NULL,
    "created"     	    TIMESTAMP    NOT NULL,
    "updated"     	    TIMESTAMP    DEFAULT NULL,
    "title"   	        VARCHAR      NOT NULL,

    CONSTRAINT expense_categories_pkey
    PRIMARY KEY(id)
);


CREATE TABLE transactions(
    "id"                SERIAL       NOT NULL,
    "created"           TIMESTAMP    NOT NULL,
    "updated"           TIMESTAMP    DEFAULT NULL,
    "accountFrom"       INT(10)      NOT NULL,
    "accountTo"         INT(10)      NOT NULL,
    "transactionDate"   TIMESTAMP    NOT NULL,
    "transactionSum"    FLOAT        DEFAULT NULL,
    "sum"               FLOAT        NOT NULL,
    "limitBalance"      FLOAT        DEFAULT NULL,
    "limitExceeded"     BOOLEAN      DEFAULT NULL DEFAULT FALSE,
    "currency_id"       INT          NOT NULL,
    "expense_id"        INT          NOT NULL,


    CONSTRAINT transactions_pkey
        PRIMARY KEY(id),

    CONSTRAINT fk_transaction_currency_category
        FOREIGN KEY (currency_id)
            REFERENCES currency_categories (id)
            ON DELETE RESTRICT
            ON UPDATE CASCADE,

    CONSTRAINT fk_transaction_expense_category
        FOREIGN KEY (expense_id)
            REFERENCES expense_categories (id)
            ON DELETE RESTRICT
            ON UPDATE CASCADE
);


--Модуль курсы валют.

CREATE TABLE current_currencies
(
    "id"          	    SERIAL       NOT NULL,
    "created"     	    TIMESTAMP    NOT NULL,
    "updated"     	    TIMESTAMP    DEFAULT NULL,
    "title"   	        VARCHAR      NOT NULL,

    CONSTRAINT current_currencies_pkey
        PRIMARY KEY(id)
);

--Модуль счета

CREATE TABLE accounts
(
    "id"          	    SERIAL       NOT NULL,
    "created"     	    TIMESTAMP    NOT NULL,
    "updated"     	    TIMESTAMP    DEFAULT NULL,
    "number"   	        INT(10)      NOT NULL,
    "limit_sum"         FLOAT        DEFAULT NULL DEFAULT 0,
    "limitDate"         TIMESTAMP    DEFAULT NULL,
    "expense_id"        INT          NOT NULL,



    CONSTRAINT accounts_pkey
        PRIMARY KEY(id),


    CONSTRAINT fk_account_expense_category
        FOREIGN KEY (expense_id)
            REFERENCES expense_categories (id)
            ON DELETE RESTRICT
            ON UPDATE CASCADE
);





